## Arduino code for circular pattern LED Blink

```
void setup()                            //function to setup pins 2-6 as output pins
{
   for(int i=2; i<=6; i++)              // counting the variable i from 2 to 6
   pinMode(i,OUTPUT);                   // initialising the led at index i of the array of led as OUTPUT
}

void loop(){                           //main function to run forever

   for(int i=0; i<=5; i++) {
   pat1();}
  
for(int i=0; i<=5; i++) {
   pat2();}
  
}
  
void pat1(){                            //function for faster pattern(left to right)
    for(int i=2; i<=6; i++) {
      digitalWrite(i,HIGH);             //LED at index i will glow           
      delay(100);                       //wait for 100 ms
      digitalWrite(i,LOW);
 
    }
    
    for(int i=5; i>=3; i--) {          // right to left
      digitalWrite(i,HIGH);
      delay(100);
      digitalWrite(i,LOW);
    }
} 

void pat2(){                            // function for slower pattern(left to right)
    for(int i=2; i<=6; i++) {
      digitalWrite(i,HIGH);
      delay(1000);                      //wait for 1 s
      digitalWrite(i,LOW);
 
    }
    
    for(int i=5; i>=3; i--) {          //right to left
      digitalWrite(i,HIGH);
      delay(1000);
      digitalWrite(i,LOW);
    }
} 
```