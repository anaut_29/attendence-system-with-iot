# 20 Hour Learning

Scientifically,the more we `practice` the the more we `learn` and takes less time to perform a specific task.

![img](images/cap1.JPG)

It takes 10,000 hours to achieve mastery in a field.  But it only takes 20 hours to get good at something, if you practice intelligently.

![img](images/cap2.JPG)

> “You can go from knowing nothing about any skill that you can think of.  Want to learn a language? Want to learn how to draw? Want to learn how to juggle flaming chainsaws?If you put 20 hours of focused deliberate practice into that thing,  you will be astounded.  Astounded at how good you are.  20 hours is doable, that’s about 45 minutes a day for about a month.  Even skipping a couple days, here and there.” 

## 1. Deconstruct the skill

![img](images/cap3.JPG)

* Break it down.  If you want to learn a new skill, the first thing you need to do is break it down into small action steps.

##  2. Learn Enough to Self-Correct

![img](images/cap6.JPG)

* Take action and get started.  Start practicing the new skill you want to learn so that you can generate feedback and start correcting yourself.  You need to start to recognize what good looks like or what good feels like, so you can change your approach as necessary.  This is how you will create your learning loop.

## 3. Remove Practice Barriers

![img](images/cap4.JPG)

* Get rid of whatever gets in the way of your ability and motivation to practice.  Make it easier to practice, by getting rid of distractions.

## 4. Practice at least 20 hours

![img](images/cap5.JPG)

* If you can invest 20 hours to learn your new skill, this will help you get over the initial “frustration barrier.”   It’s this “frustration barrier” that holds so many people back from learning new things, exploring new interests, or realizing their potential.
