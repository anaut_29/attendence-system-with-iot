# What is RASPBERRY Pi

1. The Raspberry Pi is an inexpensive and miniature computer.

2. It was developed as an educational tool, and has been made available to everyone, no matter your computer expertise.

3. The Raspberry Pi is a versatile computer that can be made into just about anything.

4. If you’ve been wanting an inexpensive way to get into programming and DIY tech projects, this is an awesome place to start!

## Specifications(Raspberry Pi 4)
* Broadcom BCM2711, Quad core Cortex-A72 (ARM v8) 64-bit SoC @ 1.5GHz
* 2GB, 4GB or 8GB LPDDR4-3200 SDRAM (depending on model)
* 2.4 GHz and 5.0 GHz IEEE 802.11ac wireless, Bluetooth 5.0, BLE Gigabit Ethernet
* 2 USB 3.0 ports; 2 USB 2.0 ports.
* Raspberry Pi standard 40 pin GPIO header (fully backwards compatible with previous boards)
* 2 × micro-HDMI ports (up to 4kp60 supported)
* 2-lane MIPI DSI display port
* 2-lane MIPI CSI camera port
* 4-pole stereo audio and composite video port
* H.265 (4kp60 decode), H264 (1080p60 decode, 1080p30 encode)
* OpenGL ES 3.0 graphics
* Micro-SD card slot for loading operating system and data storage
* 5V DC via USB-C connector (minimum 3A*)
* 5V DC via GPIO header (minimum 3A*)
* Power over Ethernet (PoE) enabled (requires separate PoE HAT) Operating temperature: 0 – 50 degrees C ambient
* A good quality 2.5A power supply can be used if downstream USB peripherals consume less than 500mA in total.

## Raspberry Pi

![img](images/pi-labelled-names.png)

You are going to take a first look at Raspberry Pi! You should have a Raspberry Pi computer in front of you for this. The computer shouldn’t be connected to anything yet.

### USB ports — 

![img1](images/pi-keyboard.png)

* These are used to connect a mouse and keyboard. You can also connect other components, such as a USB drive.

### SD card slot — 

![img2](images/pi-sd.png)

* You can slot the SD card in here. This is where the operating system software and your files are stored.

### Ethernet port — 

![img3](images/pi-ethernet.png)

* This is used to connect Raspberry Pi to a network with a cable. Raspberry Pi can also connect to a network via wireless LAN.

### Audio jack — 

![img4](images/pi-headphones.png)

* You can connect headphones or speakers here.

### HDMI port — 

![img5](images/pi-hdmi-2.png)

* This is where you connect the monitor (or projector) that you are using to display the output from the Raspberry Pi. If your monitor has speakers, you can also use them to hear sound.

### Micro USB power connector — 

![img6](images/pi-power.png)

* This is where you connect a power supply. You should always do this last, after you have connected all your other components.

### GPIO ports — 

* These allow you to connect electronic components such as LEDs and buttons to Raspberry Pi.
 
<img src='images/6sQiFTKXhZptFiGnPlsc.png' height='400px'>


