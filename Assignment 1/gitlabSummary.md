# Git workflow

There are three main components of a Git project:

## Repository

* The `repository`, or `repo`, is the “container” that tracks the changes to your project files.
* It holds all the commits—a snapshot of all your files at a point in time—that have been made.
* You can access the commit history with the Git log.

## Working tree

* The `working tree`, or `working directory`, consists of files that you are currently working on.
* You can think of a working tree as a file system where you can view and modify files.

## Index

* The `index`, or `staging area`, is where commits are prepared.
* The index compares the files in the working tree to the files in the repo.
* When you make a change in the working tree, the index marks the file as modified before it is committed.

![img1](images/git_workflow_001.png)

## Below is the basic Git workflow:

1. Modify files in the working tree.
2. Stage the changes you want to be included in the next commit.
3. Commit changes. Committing will take the files from the index and store them as a snapshot in the repository.

## Three states of Git files

As you can probably guess from the Git workflow, files can be in one of three states:

* Modified
* Staged
* Committed

![img1](images/git_workflow_002.png)
The three file states for Git: modified, staged, and committed.

# Required Vocabulary

## Commit

* When you commit to a repository, it’s like you’re gathering up the files as they exist at that moment and putting them in a time capsule.
* The commit will only exist on your local machine until it is pushed to a remote repository.

## Push

* Pushing is essentially syncing your commits to the cloud.

## Branch

* Think of your git repo as a tree.
* The trunk of the tree, is called the Master Branch. That’s the one that goes live.
* Branches are separate instances of the code that offshoots from the main codebase.

## Merge

* Integrating two branches together is called Merge.

## Clone

* Cloning takes the entire online repository and makes an exact copy of it on your local machine.

## Fork

* Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

## Pull Request

* A pull request is when you submit a request for the changes you have made to be pulled (or merged) into the Master Branch of the repository.

### Basic GIT commands

| Function | Command |
| ----------- | ----------- |
| Configuring username and email  | git config –global user.name “[username]” |
|    |git config –global user.email “[email address]” |
|New Repository|git init [Repository Name]|
|Cloning|git clone [repository url]|
|Adding file to staging area|git add [file name]|
|Commiting|git commit -m "Commit message"|
|Show files to be commited|git status|
|Switching Branch|git checkout [Branch name]|
|Commit change to remote|git push|
|Fetch and save changes to local|git pull|

